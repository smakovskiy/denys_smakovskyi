package ua.epam;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class HashCalculator2 {

        public static String hash(char toHash[]) {
            Hasher hasher = Hashing.md5().newHasher();
            for( int i=0; i<toHash.length; i++) {
                hasher.putChar(toHash[i]);

            }
            return hasher.hash().toString();
        }

}
