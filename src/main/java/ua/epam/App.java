package ua.epam;


import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App 
{
    public static int MAX_LENGTH = 5;
    private final static String ALPHABET_M = "abcdefghijklmnopqrstuvwxyz";
    private final static String HASH_TO_FIND ="69c459dd76c6198f72f0c20ddd3c9447";
    private static long startTime;

    private  static Thread[] threads = new Thread[ALPHABET_M.length()];
    public static void main( String[] args )
    {
        startTime = System.nanoTime();
        for(int lenght=1; lenght<=MAX_LENGTH; lenght++){
           // boolean success = bruteForLength(lenght);
            permutation("", lenght);
        }
        System.out.println( "Hello World!" );
    }

  /*  private static boolean bruteForLength(int lenght) {

        char buffer[] = new char[lenght];
        for(int symbol = 0; symbol < lenght; symbol ++ ){
            for( char letter = 'a' ; letter < 'z'; letter ++ ){
                buffer[symbol] = letter;
            }
        }
    }*/

    private static void permutation(final String prefix, int length)
    {
//        if( "zebra".equals(prefix)){
//            System.exit(0);
//        }
        if( Thread.currentThread().isInterrupted()){
            System.exit(0);
        }
        if (length == 0){

            String result;
            result=HashCalculator.hash( prefix);

            if( HASH_TO_FIND.equals(result) )
            {
                long duration = System.nanoTime() - startTime;

                System.out.println("Finded key: " + result + " " + prefix);

                System.out.println("Time: " + ((double)duration / 1000000000.0) + " seconds");
                Stream.of( threads ).filter(thread -> thread != null ).forEach( Thread::interrupt );
                System.exit(0);
            }

            //System.out.println(prefix);
        }
        else if( length == 1){
            for( int i = 0; i<ALPHABET_M.length(); i++ ){
                final int localI = i;
                final String localString = new String( prefix );
                threads[localI] = new Thread( () -> {
                    permutation(localString + ALPHABET_M.charAt(localI), length-1);
                });
                threads[localI].start();
            }
        }
        else {
            for (int i = 0; i < ALPHABET_M.length(); i++){
                permutation(prefix + ALPHABET_M.charAt(i), length-1);
            }
        }
    }
}
